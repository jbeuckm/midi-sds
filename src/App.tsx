import React, { useState } from 'react'
import MidiPortSelectors from './MidiPortSelectors'
import WaveView from './features/WaveView'

function App() {
  const [file, setFile] = useState()

  const fileInputChange = ({
    nativeEvent: {
      target: { files },
    },
  }: any) => {
    const file = files[0]
    setFile(file)
  }

  return (
    <div className="App">
      <div style={{ display: 'flex', flexDirection: 'row' }}>
        <div style={{ flex: 1 }} />
        <MidiPortSelectors />
      </div>

      <div>
        <input type="file" accept="audio/wave" onChange={fileInputChange} />
      </div>

      <div>
        <WaveView file={file} />
      </div>
    </div>
  )
}

export default App
