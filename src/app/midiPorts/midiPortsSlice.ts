import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RootState } from '../store'

interface MidiPortsState {
  input: number
  output: number
}

const initialState: MidiPortsState = {
  input: 0,
  output: 0,
}

export const midiPortsSlice = createSlice({
  name: 'midiPorts',
  initialState,
  reducers: {
    setInput: (state, action: PayloadAction<number>) => {
      state.input = action.payload
    },
    setOutput: (state, action: PayloadAction<number>) => {
      state.output = action.payload
    },
  },
})

export const { setInput, setOutput } = midiPortsSlice.actions

export const selectInput = (state: RootState) => state.midiPorts.input
export const selectOutput = (state: RootState) => state.midiPorts.output

export default midiPortsSlice.reducer
