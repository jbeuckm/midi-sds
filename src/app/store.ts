import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web

import counterReducer from '../features/counter/counterSlice'
import midiPortsReducer from './midiPorts/midiPortsSlice'

const store = configureStore({
  reducer: {
    counter: counterReducer,
    midiPorts: persistReducer(
      {
        key: 'midiPorts',
        storage,
      },
      midiPortsReducer
    ),
  },
})

export default () => {
  const persistor = persistStore(store)

  return { store, persistor }
}

export type RootState = ReturnType<typeof store.getState>
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>
