import React, { useRef, useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { selectInput, selectOutput } from '../../app/midiPorts/midiPortsSlice'
import SdsService from '../../services/SdsService'
import SY99Service from '../../services/SY99Service'
import { GrPlayFill } from 'react-icons/gr'
import { AiOutlineColumnWidth } from 'react-icons/ai'
import { copy, bufferToWave } from './utils'
import KeySelect from './KeySelect'

const SegmentView = ({ sourceWavesurfer, region, initialNumber, initialName, onZoomToRegion }) => {
  const waveformRef = useRef(null)
  const wavesurfer = useRef(null)

  const [loopType, setLoopType] = useState('none')
  const [loopRegion, setLoopRegion] = useState()
  const [sampleNumber, setSampleNumber] = useState(initialNumber)
  const [sampleName, setSampleName] = useState(initialName)
  const input = useSelector(selectInput)
  const output = useSelector(selectOutput)

  const [loKey, setLoKey] = useState(60)
  const [origKey, setOrigKey] = useState(60)
  const [hiKey, setHiKey] = useState(60)

  const changeLoopType = (event) => {
    setLoopType(event.target.value)
  }

  const updateWavesurfer = () => {
    try {
      const segmentDuration = region.end - region.start
      var originalBuffer = sourceWavesurfer.current.backend.buffer
      const length = Math.floor(segmentDuration * originalBuffer.sampleRate)

      const segmentBuffer = copy(region, sourceWavesurfer.current)

      const wav = bufferToWave(segmentBuffer, 0, length)

      wavesurfer.current.loadBlob(wav)
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    wavesurfer.current = window.WaveSurfer.create({
      container: waveformRef.current,
      waveColor: '#666',
      progressColor: '#000',
      pixelRatio: 1,
      height: 100,
      // backend: 'MediaElement',
      plugins: [
        window.WaveSurfer.regions.create({
          dragSelection: {
            slop: 5,
          },
        }),
      ],
    })

    updateWavesurfer()

    wavesurfer.current.on('ready', () => {
      wavesurfer.current.disableDragSelection()
      wavesurfer.current.on('region-created', updateLoopRegion)
      wavesurfer.current.on('region-update-end', updateLoopRegion)

      addDefaultLoop()
    })

    return () => {
      wavesurfer.current.un('region-created', updateLoopRegion)
      wavesurfer.current.un('region-update-end', updateLoopRegion)
      wavesurfer.current && wavesurfer.current.destroy()
    }
  }, [])

  const updateLoopRegion = (updatedRegion) => {
    setLoopRegion(updatedRegion)
  }

  const addDefaultLoop = () => {
    wavesurfer.current.clearRegions()
    wavesurfer.current.addRegion({
      start: 0,
      end: wavesurfer.current.getDuration(),
      color: '#99999944',
      handleStyle: { left: { width: '1px' }, right: { width: '1px' } },
    })
  }

  useEffect(() => {
    updateWavesurfer()
  }, [region.start, region.end])

  const onClickSend = () => {
    const buffer = wavesurfer.current.backend.buffer
    const floatSamples = buffer.getChannelData(0)

    const uint16Samples = new Uint16Array(floatSamples.length)

    for (let i = 0, l = floatSamples.length; i < l; i++) {
      uint16Samples[i] = floatSamples[i] * 0x7fff
    }

    const loopStart = Math.floor(loopRegion.start * buffer.sampleRate)
    const loopEnd = Math.floor(loopRegion.end * buffer.sampleRate)

    const configuration = {
      bitDepth: 16,
      sampleRate: buffer.sampleRate,
      length: uint16Samples.length,
      loopStart,
      loopEnd,
      loopType,
    }
    console.log(configuration)

    // @ts-ignore
    SdsService.send(input, output, sampleNumber, { configuration, samples: uint16Samples })
  }

  const onSendSampleParams = () => {
    SY99Service.sendSampleConfig(
      output,
      sampleNumber,
      sampleName,
      loKey - 1,
      origKey,
      hiKey - 1,
      origKey
    )
    // SY99Service.requestSampleConfigs(output, input)
  }

  const handleChangeSampleNumber = (e: any) => {
    setSampleNumber(e.nativeEvent.target.value)
  }

  const handleChangeName = (e: any) => {
    setSampleName(e.nativeEvent.target.value.substring(0, 8))
  }

  return (
    <tr style={{ backgroundColor: region.color.toString(), paddingTop: 5 }}>
      <td style={{ padding: 10 }}>
        <button onClick={() => region.remove()}>x</button>
      </td>

      <td style={{ fontSize: 28, padding: 10 }} onClick={() => onZoomToRegion(region)}>
        <AiOutlineColumnWidth />
      </td>

      <td style={{ border: '1px solid #999', whiteSpace: 'nowrap' }}>
        <div>
          <label style={{ padding: 5 }} htmlFor="sampleNumber">
            samplenum
          </label>
          <input
            type="number"
            name="sampleNumber"
            onChange={handleChangeSampleNumber}
            value={sampleNumber}
            style={{ width: 40 }}
            min={0}
            max={99}
            step={1}
          />
        </div>
        <div>
          <select value={loopType} onChange={changeLoopType}>
            <option value="forward">forward</option>
            <option value="backward/forward">backward/forward</option>
            <option value="none">none</option>
          </select>
          <button style={{ padding: 5 }} onClick={() => onClickSend(region)}>
            send
          </button>
        </div>
      </td>
      <td style={{ padding: 10 }}>
        <button onClick={() => wavesurfer.current.play(0)}>
          <GrPlayFill />
        </button>
      </td>
      <td style={{ border: '1px solid #999' }}>
        <table>
          <thead>
            <tr>
              <th>lo</th>
              <th>orig</th>
              <th>hi</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <KeySelect value={loKey} onChange={(event) => setLoKey(event.target.value)} />
              </td>
              <td>
                <KeySelect value={origKey} onChange={(event) => setOrigKey(event.target.value)} />
              </td>
              <td>
                <KeySelect value={hiKey} onChange={(event) => setHiKey(event.target.value)} />
              </td>
            </tr>
          </tbody>
        </table>

        <div style={{ flexDirection: 'row', flex: 1 }}>
          <input onChange={handleChangeName} value={sampleName} maxLength={8} size={8} />
          <span style={{ flex: 1 }} />
          <button onClick={onSendSampleParams}>send</button>
        </div>
      </td>

      <td style={{ width: '100%' }}>
        <div style={{ width: '100%' }} ref={waveformRef} />
      </td>
    </tr>
  )
}

export default SegmentView
