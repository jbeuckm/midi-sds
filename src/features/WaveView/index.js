import React, { ChangeEvent, useRef, useEffect, useState } from 'react'
import SegmentView from './SegmentView'
import distinctColors from 'distinct-colors'
import { path } from 'ramda'
import { GrPlayFill, GrPauseFill } from 'react-icons/gr'

const MAX_ZOOM = 10000

var palette = distinctColors({ count: 6, chromaMin: 50 })
let zoom = 1

const WaveView = ({ file }) => {
  const waveformRef = useRef(null)
  const wavesurfer = useRef(null)
  const zoomSlider = useRef(null)
  const [nextColor, setNextColor] = useState(0)
  const [regions, setRegions] = useState()
  const [zoomMin, setZoomMin] = useState()
  const [playing, setPlaying] = useState(false)

  useEffect(() => {
    wavesurfer.current = window.WaveSurfer.create({
      container: waveformRef.current,
      height: 80,
      waveColor: '#666',
      progressColor: '#000',
      pixelRatio: 1,
      // backend: 'MediaElement',
      plugins: [
        window.WaveSurfer.regions.create({
          dragSelection: {
            slop: 5,
          },
        }),
      ],
    })

    wavesurfer.current.on('play', () => setPlaying(true))
    wavesurfer.current.on('pause', () => setPlaying(false))

    wavesurfer.current.on('error', console.log)
    wavesurfer.current.on('ready', () => {
      setZoomMin(wavesurfer.current.params.minPxPerSec)
      addRegion()
    })
    wavesurfer.current.on('region-created', handleRegionAdded)
    wavesurfer.current.on('region-update-end', updateRegions)
    wavesurfer.current.on('region-removed', updateRegions)

    waveformRef.current.addEventListener('wheel', handleWheelZoom, { passive: false })

    return () => {
      waveformRef.current && waveformRef.current.removeEventListener('wheel', handleWheelZoom)
      wavesurfer.current && wavesurfer.current.destroy()
    }
  }, [])

  const handleRegionAdded = (e: any) => {
    setImmediate(() => {
      updateRegions()
    })
  }

  useEffect(() => {
    if (!regions) return
    regions.forEach((region) => {
      if (!region.attributes.color) {
        region.color = palette[nextColor].alpha(0.3)
        setNextColor((nextColor + 1) % palette.length)
        region.attributes.color = true
      }
    })
  }, [regions])

  const updateRegions = (e: any) => {
    const regionList = path(['current', 'regions', 'list'], wavesurfer)

    setRegions(Object.values(regionList))
  }

  useEffect(() => {
    if (!file) return
    const urlReader = new FileReader()
    urlReader.onload = function () {
      const fileDataUrl = this.result

      if (fileDataUrl) {
        setNextColor(0)
        if (wavesurfer.current) {
          wavesurfer.current.clearRegions()
        }
        wavesurfer.current.load(fileDataUrl)
      }
    }
    urlReader.readAsDataURL(file)
  }, [file])

  const addRegion = () => {
    wavesurfer.current.addRegion({
      start: 0,
      end: wavesurfer.current.getDuration() / 2,
      color: palette[nextColor].alpha(0.3),
      handleStyle: { left: { width: '1px' }, right: { width: '1px' } },
      attributes: { color: true },
    })
    setNextColor((nextColor + 1) % palette.length)
  }

  const clickPlay = () => {
    wavesurfer.current.playPause()
  }

  const handleZoom = ({ nativeEvent }: ChangeEvent<HTMLInputElement>) => {
    const newZoom = Number(nativeEvent.target.value)
    wavesurfer.current.zoom(newZoom)
    zoom = newZoom
  }

  const handleWheelZoom = (event: ChangeEvent<HTMLInputElement>) => {
    event.preventDefault()
    const newZoom = Math.max(0, zoom - event.deltaY)

    wavesurfer.current.zoom(newZoom)
    zoom = newZoom
  }

  const handleZoomToRegion = (region: any) => {
    const centerTime = (region.start + region.end) / 2
    const centerProgress = centerTime / wavesurfer.current.getDuration()

    const zoomedDuration = (region.end - region.start) * 1.1
    const pixelsPerSec = waveformRef.current.offsetWidth / zoomedDuration

    wavesurfer.current.seekAndCenter(centerProgress)
    wavesurfer.current.zoom(pixelsPerSec)
  }

  return (
    <div>
      <input ref={zoomSlider} type="range" min={zoomMin} max={MAX_ZOOM} onChange={handleZoom} />

      <div ref={waveformRef} />

      <button onClick={clickPlay}>{playing ? <GrPauseFill /> : <GrPlayFill />}</button>

      <table>
        <thead>
          <tr>
            <th></th>
            <th>View</th>
            <th>MIDI SDS</th>
            <th></th>
            <th>SY99 Params</th>
          </tr>
        </thead>
        <tbody>
          {regions &&
            regions.map((region, index) => {
              return (
                <SegmentView
                  key={region.id}
                  region={region}
                  sourceWavesurfer={wavesurfer}
                  initialNumber={index}
                  initialName={file.name.slice(0, 7) + index}
                  onZoomToRegion={handleZoomToRegion}
                />
              )
            })}
        </tbody>
      </table>
    </div>
  )
}

export default WaveView
