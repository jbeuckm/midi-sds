import React from 'react'

const KEY_NAMES = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']

const keys = Array(128)
  .fill(0)
  .map((_, index) => {
    const octave = Math.floor(index / 12)
    const key = index % 12

    return `${KEY_NAMES[key]}${-2 + octave}`
  })

const KeySelect = (props) => (
  <select {...props}>
    {keys.map((key, index) => (
      <option key={index} value={index}>
        {key}
      </option>
    ))}
  </select>
)

export default KeySelect
