import WebMidi from 'webmidi'
import Sysex from 'sysex'
import sendSampleParams from './sy99formats/sendSampleParams'

class SY99Service {
  static handleSysex(e: any) {
    console.log('handleSysex', e.data)
  }

  static sendSampleConfig(
    output: number,
    sampleNumber: number,
    name: string,
    lowKeyCode: number,
    originalKey: number,
    hiKeyCode: number,
    pitchCode: number
  ) {
    const sysex = new Sysex(sendSampleParams)

    const bytes = sysex.encode({
      sampleNumber,
      sampleName: name,
      lowKeyCode,
      hiKeyCode,
      originalKey,
      pitchCode,
    })

    const checksum = bytes.slice(4).reduce((acc, item) => +acc + +item, 0)

    const midiOutput = WebMidi.outputs[output]
    if (!midiOutput) {
      return alert('Please select midi output.')
    }

    midiOutput.sendSysex(0x43, [...bytes, (~checksum + 1) & 0x7f])
  }

  static requestSampleConfigs(output: number, input: number) {
    const message = [
      0x20,
      0x7a,
      0x4c,
      0x4d,

      0x20,
      0x20,
      0x30,
      0x30,

      0x34,
      0x30,
      0x53,
      0x41,

      0x00,
      0x00,
      0x00,
      0x00,

      0x00,
      0x00,
      0x00,
      0x00,

      0x00,
      0x00,
      0x00,
      0x00,

      0x00,
      0x00,
      0x00,
      0x00,
    ]

    WebMidi.inputs[input].addListener('sysex', 'all', SY99Service.handleSysex)
    WebMidi.outputs[output].sendSysex(0x43, message)
  }
}

export default SY99Service
