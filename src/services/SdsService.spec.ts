jest.mock('webmidi', () => ({ outputs: [{ sendSysex: jest.fn() }] }))

import SdsService from './SdsService'

describe('SdsService', () => {
  test('builds SDS header', () => {
    SdsService.sendDumpHeader(0, 0, 0, {
      bitDepth: 16,
      sampleRate: 44100,
      length: 10000,
      loopStart: 0,
      loopEnd: 0,
      loopType: 0,
    })

    const WebMidi = require('webmidi')

    expect(WebMidi.outputs[0].sendSysex).toHaveBeenCalledWith(
      [126, 0, 1],
      [0, 0, 16, 19, 49, 1, 16, 78, 0, 0, 0, 0, 0, 0, 0, 0]
    )
  })
})
