// http://www.piclist.com/techref/io/serial/midi/sds.html
import { pack3, unpack3, packSample16 } from './utils'

describe('utils', () => {
  test('pack3', () => {
    expect(pack3(23999)).toEqual([0x3f, 0x3b, 0x01])
  })
  test('unpack3', () => {
    expect(unpack3([0x3f, 0x3b, 0x01])).toEqual(23999)
    // console.log(unpack3([0x13, 0x31, 0x01])) // 22675
    // console.log(unpack3([0x57, 0x18, 0x01])) // 19543
  })
})

test('packSample16', () => {
  expect(packSample16(0xf0f0)).toEqual([0x78, 0x3c, 0x00])
})
