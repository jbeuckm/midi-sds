export const pack3 = (input: number) => {
  const lo = input & 0x7f
  const mid = (input >>> 7) & 0x7f
  const hi = (input >>> 14) & 0x7f

  return [lo, mid, hi]
}

export const unpack3 = (bytes: number[]) => {
  const [lo, mid, hi] = bytes
  return lo + (mid << 7) + (hi << 14)
}

export const packSample16 = (sample: number) => {
  const hi = (sample >>> 9) & 0x7f
  const mid = (sample >>> 2) & 0x7f
  const lo = (sample & 0x03) << 5

  return [hi, mid, lo]
}
