import WebMidi from 'webmidi'
import Sentence, { Options } from 'sysex'
import { packSample16 } from './utils'

export enum LoopType {
  FORWARD_ONLY = 0x00,
  BACKWARD_FORWARD = 0x01,
  NONE = 0x7f,
}

const dumpHeader = new Sentence({
  sentence: [
    'waveNumber[2]/ bitDepth period[3]/ lengthWords[3]/ loopStart[3]/ loopEnd[3]/ loopType',
  ],
  transcoders: {
    loopType: Options.withOptions({
      forward: [0x00],
      'backward/forward': [0x01],
      none: [0x7f],
    }),
  },
})

export type SampleConfiguration = {
  bitDepth: number
  sampleRate: number
  length: number
  loopStart?: number
  loopEnd?: number
  loopType?: LoopType
}

export type SendRequest = {
  configuration: SampleConfiguration
  samples: Uint16Array
}

const WORDS_PER_PACKET = 120 / 3

type ResponseType = 'ack' | 'nack' | 'cancel' | 'wait'
const RESPONSE_BYTE: Record<number, ResponseType> = {
  0x7f: 'ack',
  0x7e: 'nack',
  0x7d: 'cancel',
  0x7c: 'wait',
}

type ResponseMessage = { deviceId: number; packetNumber: number }
type Waiters = Record<ResponseType, Array<(message: ResponseMessage) => void>>

class SdsService {
  static waiters: Waiters = {
    ack: [],
    nack: [],
    cancel: [],
    wait: [],
  }

  static waitForNext(type: ResponseType) {
    return new Promise((resolve, reject) => {
      SdsService.waiters[type].push(resolve)
    })
  }

  static handleSysex(e: any) {
    if (e.data.length === 6 && e.data[1] === 0x7e) {
      const message = {
        deviceId: e.data[2],
        packetNumber: e.data[4],
      }

      const type = RESPONSE_BYTE[e.data[3]]

      SdsService.waiters[type].forEach((callback) => callback(message))
      SdsService.waiters[type] = []
    }
  }

  static async send(
    input: number,
    output: number,
    sampleNumber: number,
    { configuration, samples }: SendRequest
  ) {
    const midiInput = WebMidi.inputs[input]
    if (!midiInput) {
      return alert('Please select midi input.')
    }
    const midiOutput = WebMidi.outputs[output]
    if (!midiOutput) {
      return alert('Please select midi output.')
    }

    WebMidi.inputs[input].addListener('sysex', 'all', SdsService.handleSysex)

    SdsService.sendDumpHeader(output, 0, sampleNumber, { ...configuration, length: samples.length })

    await SdsService.waitForNext('ack')

    for (let i = 0; i < samples.length; i += WORDS_PER_PACKET) {
      const packetBytes = samples.slice(i, i + WORDS_PER_PACKET)
      SdsService.sendDataPacket(output, 0, i % 128, packetBytes)

      await SdsService.waitForNext('ack')
    }

    WebMidi.inputs[input].removeListener('sysex', 'all', SdsService.handleSysex)
  }

  /*
   * F0
   * 7E cc 01 // message type and device # cc
   * 00 00 // stored wave number
   * 10 // bit depth
   * 13 31 01 // sample period
   * 57 18 01 // length in words
   * 00 00 00 // loop start
   * 00 00 00 // loop end
   * 7F // loop type
   * F7
   */
  static sendDumpHeader(
    output: number,
    deviceId: number,
    waveNumber: number,
    config: SampleConfiguration
  ) {
    const headerPrefix = [0x7e, deviceId, 0x01]

    WebMidi.outputs[output].sendSysex(
      headerPrefix,
      dumpHeader.encode({
        waveNumber,
        period: 1000000000 / config.sampleRate,
        lengthWords: config.length,
        ...config,
      })
    )
  }

  // F0 7E cc 02 kk [120 bytes here] ll F7
  static sendDataPacket(
    output: number,
    deviceId: number,
    packetNumber: number,
    samples: Uint16Array
  ) {
    const headerPrefix = [0x7e, deviceId, 0x02]

    const data: number[] = []
    samples.forEach((sample) => {
      data.push(...packSample16(sample + 0x8000))
    })

    if (samples.length < WORDS_PER_PACKET) {
      const fill = WORDS_PER_PACKET - samples.length
      for (let i = 0; i < fill; i++) {
        data.push(...packSample16(0x8000))
      }
    }

    let checksum = headerPrefix[0] ^ headerPrefix[1] ^ headerPrefix[2] ^ packetNumber
    data.forEach((datatByte) => {
      checksum = checksum ^ datatByte
    })
    checksum &= 0x7f

    WebMidi.outputs[output].sendSysex(headerPrefix, [packetNumber, ...data, checksum])
  }

  static init = () =>
    new Promise((resolve, reject) => {
      WebMidi.enable((err) => {
        if (err) {
          reject(err)
        } else {
          resolve('Sysex is enabled!')
        }
      }, true)
    })

  static get outputs() {
    return WebMidi.outputs
  }
  static get inputs() {
    return WebMidi.inputs
  }
}

export default SdsService
