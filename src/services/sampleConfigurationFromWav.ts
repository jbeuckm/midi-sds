const sampleConfigurationFromWav = (wav: any) => {
  return {
    bitDepth: wav.fmt.bitsPerSample,
    sampleRate: wav.fmt.sampleRate,
    length: wav.chunkSize / 2,
  }
}

export default sampleConfigurationFromWav
