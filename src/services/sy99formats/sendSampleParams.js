import { Ascii } from 'sysex'
import { KeyTranscoder } from './KeyTranscoder'
import { PitchCodeTranscoder } from './PitchCodeTranscoder'

export default {
  sentence: [
    '00 7a 00 32',
    '4c 4d 20 20 30 30 34 30 53 41',
    '00[15] sampleNumber sampleName[8]',
    'hiKeyCode[4] originalKey pitchCode[4] loopMode volCode[2] lowKeyCode[4]',
  ],
  transcoders: {
    sampleName: Ascii,
    lowKeyCode: KeyTranscoder,
    pitchCode: PitchCodeTranscoder,
    hiKeyCode: KeyTranscoder,
  },
  defaults: {
    originalKey: [0x3c],
    hiKeyCode: [0x00, 0x2a, 0x00, 0x00],
    pitchCode: [0x00, 0x14, 0x00, 0x28],
    lowKeyCode: [0x00, 0x00, 0x00, 0x00],
  },
}
