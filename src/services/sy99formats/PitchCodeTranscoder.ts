import { Transcoder } from 'sysex'

const LOW_BYTE = [0x28, 0x7d, 0x152]

const valueForKey = (key) => 0x10000 * Math.floor(key / 3) + LOW_BYTE[key % 3]

export class PitchCodeTranscoder extends Transcoder {
  encode(key: number): number[] {
    const rawBytes = valueForKey(key)
    const bytes = []
    bytes.unshift(rawBytes & 0xff)

    bytes.unshift((rawBytes & 0xff00) >>> 8)
    bytes.unshift((rawBytes & 0xff0000) >>> 16)
    bytes.unshift((rawBytes & 0xff000000) >>> 24)

    return bytes
  }

  decode(bytes: number[]): string {
    console.log('not implemented')
  }
}
