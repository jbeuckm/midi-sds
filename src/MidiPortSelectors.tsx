import React, { useEffect, useState } from 'react'
import SdsService from './services/SdsService'

import { useSelector, useDispatch } from 'react-redux'
import { setInput, setOutput, selectInput, selectOutput } from './app/midiPorts/midiPortsSlice'

function MidiPortSelectors() {
  const [midiReady, setMidiReady] = useState(false)
  const dispatch = useDispatch()
  const input = useSelector(selectInput)
  const output = useSelector(selectOutput)

  useEffect(() => {
    SdsService.init().then(() => {
      setMidiReady(true)
    })
  }, [])

  const onChangeInput = (e: any) => {
    dispatch(setInput(e.nativeEvent.target.value))
  }
  const onChangeOutput = (e: any) => {
    dispatch(setOutput(e.nativeEvent.target.value))
  }

  if (!midiReady) {
    return null
  }

  return (
    <>
      <div style={{ padding: 3 }}>
        <label htmlFor="input">in</label>
        <span style={{ width: 15 }}> </span>
        <select name="input" onChange={onChangeInput} value={input}>
          <option value={-1}>select...</option>
          {SdsService.inputs.map((input, index) => (
            <option key={input.id} value={index}>
              {input.manufacturer} | {input.name}
            </option>
          ))}
        </select>
      </div>

      <div style={{ padding: 3 }}>
        <label htmlFor="output">out</label>
        <span style={{ width: 15 }}> </span>
        <select name="output" onChange={onChangeOutput} value={output}>
          <option value={-1}>select...</option>
          {SdsService.outputs.map((output, index) => (
            <option key={output.id} value={index}>
              {output.manufacturer} | {output.name}
            </option>
          ))}
        </select>
      </div>
    </>
  )
}

export default MidiPortSelectors
